# Project: GT-SAM Based Graph SLAM on Victoria Park Dataset #

* This is a README for EECS568: Mobile Robotics Final Project. 
* We provide a brief explanation on how to simulate 
	the GTSAM based iSAM2/EKF-SLAM algorithm on Simulated environment/Victoria Park dataset.
* More detailed explanation of the simulation results are provided in the report.

### How do I get set up? ###

* Windows: run main.m in scripts/ folder after uncommenting your desired algorithm and environment.
* OSX: 	same as above instruction. 
		In case of undefined NonlinearFactorGraph error, check the comment in the addpath section of the main.m
		
### Important Files ###
* mobrob01/scripts/SAM/vicpark/SAM_vp.m 
	* Runs either batch (Levenberg Marquardt) or iterative (iSAM2) SAM solutions on Victora Park dataset
* mobrob01/scripts/SAM/slamsim/SAM_sim.m 
	* Runs either batch (Levenberg Marquardt) or iterative (iSAM2) SAM solutions on a small robot arena testcase
* mobrob01/scripts/EKF/vicpark/runvp1.m 
	* Runs an EKF-SLAM solution on Victoria Park dataset
* mobrob01/scripts/EKF/slamsim/runsim.m 
	* Runs an EKF-SLAM solution on a small robot arena testcase
	
### Who do I talk to? ###
 * Prince Kuevor 	- kuevpr@umich.edu
 * Youngwoo Cho 	- youngcho@umich.edu
 * Haoyu Yu 		- yuhaoy@umich.edu
 * Xi Chen 			- cxchance@umich.edu
 * Ping Yu 			- yuping@umich.edu
 
 