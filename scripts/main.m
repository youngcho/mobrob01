%% Select an Algorithm and Dataset
% alg   - 'EKF' or 'SAM'
% data  - 'sim' or 'vp'

alg = 'EKF';
% alg = 'SAM';

% data = 'sim';
data = 'vp';

%% Import SAM Stuff

% Different library paths to add depending on OS
mfilepath=fileparts(which('main'));
if ismac
    % if undefined NonlinearFactorGraph error, 
    % toggle semicolon at the end of addpath command
    addpath(fullfile(mfilepath,'../osx/gtsam_toolbox'))
elseif ispc
    addpath(fullfile(mfilepath,'../windows/gtsam_toolbox'));
end


%% Execute
if(strcmp(alg, 'SAM'))
    
    if(strcmp(data,'vp'))
        run('./SAM/vicpark/SAM_vp.m')
        
    elseif(strcmp(data,'sim'))
        run('./SAM/slamsim/SAM_sim.m')
        
    end
    
    
elseif(strcmp(alg, 'EKF'))
    addpath('./EKF/');
    mainEKF(600, data, 0, 'nn');
    
end

