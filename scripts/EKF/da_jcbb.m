function Li = da_jcbb(z, R)
% perform joint-compatability branch and bound data association

global Param;
global State;
[num_row, num] = size(z);

if State.Ekf.nl==0
    Li = zeros(1,num);  
else

MD2 = computeMD2(z,R);
compatibility.IC = MD2<chi2inv(0.99,2);
compatibility.AL = (sum (compatibility.IC,2))';

N = jcbb(z,R,compatibility,[],1);
Li = N;
end
if (size(find(Li),2) ~= 0)
    Stop = 0;
end

end






function N = jcbb(z,R,compatibility,M,j)
global Param;
global State;
    N = [];
    lenz = size(z,2);
    if j > lenz
        N = M;
    else
        individuallyCompatible = find(compatibility.IC(j, :));
        for i = individuallyCompatible
            if(jointCompatible(z,R,[M i])==1)
                N = jcbb(z,R,compatibility,[M i],j+1);
%                 break
            end
        end

    if pairings(M)+pairings(compatibility.AL(j+1:end))>=pairings(N)
        N = jcbb(z,R,compatibility,[M 0], j+1);
    end
    end
end

function num = pairings(M)
    num = length(find(M));
end

function MD2 = computeMD2(z,R)
global Param;
global State;
    lenz = size(z,2);
    MD2 = zeros(lenz,State.Ekf.nl);
    for i=1:State.Ekf.nl
        number_x = 2*i+2;
        number_y = 2*i+3;
        for j = 1:lenz
            yita = [State.Ekf.mu(number_x) - State.Ekf.mu(1); State.Ekf.mu(number_y) - State.Ekf.mu(2)];
            q = yita'*yita;
            F = [[eye(3);zeros(2,3)] zeros(5,2*i-2) [zeros(3,2);eye(2)] zeros(5, 2*State.Ekf.nl-2*i)];
            H_low = 1/q*[-sqrt(q)*yita(1) -sqrt(q)*yita(2) 0 sqrt(q)*yita(1) sqrt(q)*yita(2);...
            yita(2) -yita(1) -q -yita(2) yita(1)];
            H = H_low*F;
            s = H*State.Ekf.Sigma*H'+Param.R;
            z_hat = [sqrt(q); atan2(yita(2),yita(1))-State.Ekf.mu(3)];
            z_diff = [z_hat(1) - z(1,j); minimizedAngle(z_hat(2)-z(2,j))];
            Distance = (z_diff)'*1/s*(z_diff);
            MD2(j,i) = Distance;
        end
    end
end



function answer = jointCompatible (z, R, N)
global Param;
global State;
    JMD2 = jointMahalanobis2 (z, N, R);
    dof = 2*length(find(N));
    answer = JMD2 < chi2inv(0.99,dof);
end

function JMD2 = jointMahalanobis2 (z, N, R)
global Param;
global State;
    k = find(N);
    Hi = [];
    z_hat_ii = [];
    z_diff_ii = [];
    Ri = zeros(2*size(find(N),2));
    Ri(1:2,1:2) = R(1:2,1:2);
    if size(find(N),2)>=1
        for num_R = 1:size(find(N),2)-1
        Ri(2*num_R+1,2*num_R+1) = Param.R(1,1);
        Ri(2*num_R+2,2*num_R+2) = Param.R(2,2);
        end
    end
 
    
    for i = k
        %j represent the landmark 
        j = N(i);
        number_x = 2*j+2;
        number_y = 2*j+3;
        yita = [State.Ekf.mu(number_x) - State.Ekf.mu(1); State.Ekf.mu(number_y) - State.Ekf.mu(2)];
        q = yita'*yita;
        F = [[eye(3);zeros(2,3)] zeros(5,2*j-2) [zeros(3,2);eye(2)] zeros(5, 2*State.Ekf.nl-2*j)];
        H_low = 1/q*[-sqrt(q)*yita(1) -sqrt(q)*yita(2) 0 sqrt(q)*yita(1) sqrt(q)*yita(2);...
            yita(2) -yita(1) -q -yita(2) yita(1)];
        H = H_low*F;
        Hi = [Hi;H];
        z_hat = [sqrt(q); atan2(yita(2),yita(1))-State.Ekf.mu(3)];
        z_hat_ii = [z_hat_ii;z_hat];
        z_diff = [z_hat(1) - z(1,i); minimizedAngle(z_hat(2)-z(2,i))];
        z_diff_ii = [z_diff_ii;z_diff];
    end
    s = Hi*State.Ekf.Sigma*Hi'+Ri;
    JMD2 = (z_diff_ii)'*1/s*(z_diff_ii);  
end
