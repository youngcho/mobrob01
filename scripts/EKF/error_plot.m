clc
clear
% addpath('./vicpark');
% [record_XYT,record_GPS,XYT,Data]=runvp(700);
% num = size(record_GPS,2);
% error=zeros(1,num);
% for i=1:num
%     x_error=Data.Gps.x(record_GPS(i))-XYT(record_XYT(i),1);
%     y_error=Data.Gps.y(record_GPS(i))-XYT(record_XYT(i),2);
%     error(i)=sqrt(x_error^2+y_error^2);
% end
% figure
% plot(1:num,error)


addpath('./vicpark');
[record_XYT,record_GPS,Data]=run(700,'vp',0,'nn');
%[record_XYT,record_GPS,Data]=runvp1(200);
num = size(record_GPS,2);
error=zeros(1,num);
for i=1:num
    x_error=Data.Gps.x(record_GPS(i))-record_XYT(1,i);
    y_error=Data.Gps.y(record_GPS(i))-record_XYT(2,i);
    error(i)=sqrt(x_error^2+y_error^2);
end
figure
plot(1:num,error)