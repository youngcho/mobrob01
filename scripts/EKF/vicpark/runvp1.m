% function [predict_time,update_time,number_landmark] = runvp(nSteps,pauseLen)
% function [varargout,result, real_result,sigma_result] = runvp(nSteps,pauseLen)
function [traj,record_GPS,Data]=runvp1(nSteps,pauseLen)

% function runvp(nSteps,pauseLen)

global Param;
global State;
global Data;


record_GPS=[];
traj=[];
result = [];
real_result = [];
sigma_result = [];


makeVideo = 0;
pauseLen = 0.1;
Param.updateMethod = 'seq';

if ~exist('nSteps','var') || isempty(nSteps)
    nSteps = inf;
end

if ~exist('pauseLen','var')
    pauseLen = 0.1; % seconds
end

if makeVideo
    try
        votype = 'avifile';
        vo = avifile('video.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);
    end
end

Data = load_vp_si();

% Initalize Params
%===================================================
% vehicle geometry
Param.a = 3.78; % [m]
Param.b = 0.50; % [m]
Param.L = 2.83; % [m]
Param.H = 0.76; % [m]

% 2x2 process noise on control input
sigma.vc = 0.02; % [m/s]
sigma.alpha = 2*pi/180; % [rad]
Param.Qu = diag([sigma.vc, sigma.alpha].^2);

% 3x3 process noise on model error
sigma.x = 0.1; % [m]
sigma.y = 0.1; % [m]
sigma.phi = 0.5*pi/180; % [rad]
Param.Qf = diag([sigma.x, sigma.y, sigma.phi].^2);

% 2x2 observation noise
sigma.r = 0.05; % [m]
sigma.beta = 1*pi/180; % [rad]
Param.R = diag([sigma.r, sigma.beta].^2);
%===================================================

% Initialize State
%===================================================
State.Ekf.mu = [Data.Gps.x(2), Data.Gps.y(2), 36*pi/180]';
State.Ekf.Sigma = zeros(3);
State.Ekf.nl = 0;
State.Ekf.record = [];
State.Ekf.number = 0;
predict_time = zeros(1,nSteps);
update_time = zeros(1,nSteps);
number_landmark = zeros(1,nSteps);


global AAr;
AAr = [0:360]*pi/360;

%Computation Time
compTime = zeros(3, nSteps);

%X, Y, and Theta covariance
threeSigVals = zeros(3, 6000); %6000 is upper bound on number of inputs (ci) for what we chose to simulate
robotTraj = zeros(3, 6000);


figure(1); clf;
axis equal;

ci = 1; % control index
t = min(Data.Laser.time(1), Data.Control.time(1));
for k=1:min(nSteps, length(Data.Laser.time))
    predict_start = tic;
    while (Data.Control.time(ci) < Data.Laser.time(k))
       % control available
       dt = Data.Control.time(ci) - t;
       t = Data.Control.time(ci);
       u = [Data.Control.ve(ci), Data.Control.alpha(ci)]';
       ekfpredict_vp(u, dt);
       
       threeSigVals(:, ci) = 3*sqrt(diag(State.Ekf.Sigma(1:3,1:3)));
       robotTraj(:,ci) = State.Ekf.mu(1:3);
       
       
       ci = ci+1;
    end
    compTime(1,k) = toc(predict_start);
    predict_time(k) = toc(predict_start);
    
    % observation available
    dt = Data.Laser.time(k) - t;
    t = Data.Laser.time(k);
    z = detectTreesI16(Data.Laser.ranges(k,:));
    update_start = tic;
    if ~isempty(z)
        z = z(1:2,:);
        z(2,:) = z(2,:) - pi/2;
        nnTime = ekfupdate(z);
    end
    compTime(3,k) = toc(update_start);
    compTime(2,k) = nnTime;
    update_time(k) = toc(update_start);
    number_landmark(k) = State.Ekf.nl;
    threeSigVals(:, ci) = 3*sqrt(diag(State.Ekf.Sigma(1:3,1:3)));
    robotTraj(:,ci) = State.Ekf.mu(1:3);
    
%     result = [result,State.Ekf.mu(1:3)];
%     real_result = [real_result,[Data.Sim.realRobot(1,t);Data.Sim.realRobot(2,t);Data.Sim.realRobot(3,t)]];
%     sigma_result = [sigma_result,[State.Ekf.Sigma(1,1);State.Ekf.Sigma(2,2);State.Ekf.Sigma(3,3)]];
    
    
    traj = [traj, [State.Ekf.mu(1); State.Ekf.mu(2)]];
    %record_XYT=[record_XYT,size(traj,1)];
    for i = 1 : length(Data.Gps.time)
      if Data.Gps.time(i) > t
        break;
      end
    end

    if i >= length(Data.Gps.time)
      warning('Gps used up\n');
    end

    gt_traj = [Data.Gps.x(1:max(1,i-1))'; Data.Gps.y(1:max(1,i-1))'];
    record_GPS=[record_GPS,i-1];

    figure(1);
    doGraphics(z, traj, gt_traj);
%     doGraphics(z);
    drawnow;
    
    
    figure(2);
    clf;
    hold on;
%     plot(1:k, compTime(1, 1:k),             'b--', 'linewidth', 2)
%     plot(1:k, sum(compTime(1:2, 1:k), 1),   'r',   'linewidth', 2)
%     plot(1:k, sum(compTime(1:3, 1:k), 1),   'g-.', 'linewidth', 2)
    area([compTime(1, 1:k); compTime(2, 1:k); compTime(3, 1:k)]', ...
         'FaceColor', 'flat', 'linewidth', 1.2);
    grid;
%     ylim([0,2])
%     xlim([0,400])
    hold off;
    xlabel('Iteration Number', 'Fontsize', 17)
    ylabel('Computation Time [s]', 'Fontsize', 17)
    leg = legend('Optimizer on Odometry', 'Optimizer on Measurement', ...
          'Nearest Neighbor', 'Location', 'NorthWest');
    leg.FontSize = 17;
    drawnow;
    
    figure(3);
    gpsTimeIndx = Data.Gps.time <= t;
    set(gcf, 'rend','painters','pos',[10 10 800 800])
    ax1 = subplot(2,1,1)
    plot(Data.Control.time(1:ci), robotTraj(1, 1:ci), 'k-', 'linewidth', 2)
    hold on;
    plot(Data.Control.time(1:ci), robotTraj(1, 1:ci) + threeSigVals(1, 1:ci), 'r--', 'linewidth', 2)
    plot(Data.Control.time(1:ci), robotTraj(1, 1:ci) -threeSigVals(1, 1:ci), 'r--', 'linewidth', 2)
    greenX = plot(Data.Gps.time(gpsTimeIndx), Data.Gps.x(gpsTimeIndx), 'b.');
    greenX.LineWidth = 2;
    greenX.MarkerSize = 14;
    hold off;
    xlabel('Time [s]', 'fontsize', 15)
    ylabel('X Position [m]', 'fontsize', 15)
    set(gca,'fontsize',16) 
%     axis([30, 50, -60, -30])
    grid;

    ax2 = subplot(2,1,2)
    plot(Data.Control.time(1:ci), robotTraj(2, 1:ci), 'k-', 'linewidth', 2)
    hold on;
    plot(Data.Control.time(1:ci),robotTraj(2, 1:ci) + threeSigVals(2, 1:ci), 'r--', 'linewidth', 2)
    plot(Data.Control.time(1:ci),robotTraj(2, 1:ci) -threeSigVals(2, 1:ci), 'r--', 'linewidth', 2)
    greenY = plot(Data.Gps.time(gpsTimeIndx), Data.Gps.y(gpsTimeIndx), 'b.');
    greenY.LineWidth = 2;
    greenY.MarkerSize = 14;
    hold off;
    xlabel('Time [s]', 'fontsize', 15)
    ylabel('Y Position [m]', 'fontsize', 15)
    set(gca,'fontsize',16) 
%     axis([30, 50, -40 -10])
    grid
    
    linkaxes([ax1, ax2] , 'x')
    
    
    if makeVideo
        drawnow;
        if pauseLen > 0
            pause(pauseLen);
        end
        
        F = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
          otherwise
            error('unrecognized votype');
        end
    end
end

if makeVideo
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end

%==========================================================================
function doGraphics(z,traj, gt_traj)
% Put whatever graphics you want here for visualization
%
% WARNING: this slows down your process time, so use sparingly when trying
% to crunch the whole data set!

global Param;
global State;

% plot the robot and 3-sigma covariance ellipsoid
plotbot(State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.mu(3), 'black', 1, 'blue', 1);
hold on;

plotcov2d( State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.Sigma(1:2,1:2), 'blue', 0, 'blue', 0, 3);


% restrict view to a bounding box around the current pose
BB=30;
axis([[-BB,BB]+State.Ekf.mu(1), [-BB,BB]+State.Ekf.mu(2)]);
% axis auto

% project raw sensor detections in global frame using estimate pose
xr = State.Ekf.mu(1);
yr = State.Ekf.mu(2);
tr = State.Ekf.mu(3);
for k=1:size(z,2)
    r = z(1,k);
    b = z(2,k);
    xl = xr + r*cos(b+tr);
    yl = yr + r*sin(b+tr);
    plot([xr; xl], [yr; yl],'r',xl,yl,'r*');
end


for i = 1 : (length(State.Ekf.mu) - 3)/2

  plotcov2d( State.Ekf.mu(3+2*i-1), State.Ekf.mu(3+2*i), State.Ekf.Sigma(3+2*i-1:3+2*i, 3+2*i-1:3+2*i), 'red', 0, 'red', 0, 3 );

end

% plot trajectory
plot(traj(1,:), traj(2,:), 'k'), hold on
scatter(gt_traj(1,:), gt_traj(2,:), 'g^');


hold off;

