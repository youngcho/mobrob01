function ekfpredict_sim(u)
% EKF-SLAM prediction for simulator process model

global Param;
global State;




G = [1,0,-u(2)*sin( State.Ekf.mu(3) + u(1)); ...
    0,1, u(2)*cos( State.Ekf.mu(3) + u(1));
    0,0,1];

F = [1,0,0,zeros(1,2*State.Ekf.nl);...
    0,1,0,zeros(1,2*State.Ekf.nl);...
    0,0,1,zeros(1,2*State.Ekf.nl)];
mu_predict = State.Ekf.mu + F'*[u(2)*cos( State.Ekf.mu(3)+u(1));u(2)*sin(State.Ekf.mu(3)+u(1));u(1)+u(3)];

G_t = [G,zeros(3,2*State.Ekf.nl);...
    zeros(2*State.Ekf.nl,3),eye(2*State.Ekf.nl,2*State.Ekf.nl)];

R = [Param.alphas(1)*u(1).^2+Param.alphas(2)*u(2).^2,0,0;
        0, Param.alphas(3)*u(2).^2+Param.alphas(4)*u(1).^2+Param.alphas(4)*u(3).^2,0;
        0,0,Param.alphas(1)*u(3).^2+Param.alphas(2)*u(2).^2];
v = [-u(2)*sin(State.Ekf.mu(3)+u(1)), cos(State.Ekf.mu(3)+u(1)),0;
    u(2)*cos(State.Ekf.mu(3)+u(1)), sin(State.Ekf.mu(3)+u(1)),0;
    1,0,1];

Sigma_predict = G_t*State.Ekf.Sigma*G_t' + F'*(v*R*v')*F;

State.Ekf.mu = mu_predict;
State.Ekf.Sigma = Sigma_predict;




