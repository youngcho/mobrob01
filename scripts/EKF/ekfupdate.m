% function ekfupdate(z)
% function [record_z,Li] = ekfupdate(z)
function nnCompTime = ekfupdate(z)
% EKF-SLAM update step for both simulator and Victoria Park data set

global Param;
global State;

% z =[268.0986  140.1485;...
%    -0.1817   -0.2805;...
%     4.0000    3.0000];



% returns state vector indices pairing observations with landmarks
if strcmp(Param.updateMethod,'seq')
    nnStartTime = tic;
    switch lower(Param.dataAssociation)
    case 'known'
        Li = da_known(z(3,:));
        
    case 'nn'
        Li = da_nn(z(1:2,:));
    case 'jcbb'
        Li = da_jcbb(z(1:2,:), Param.R);
    otherwise
        error('unrecognized data association method: "%s"', Param.dataAssociation);
    end
    nnCompTime = toc(nnStartTime);
n = size(z,2);
num = size(Li,2);

Q = Param.R ;

for i=1:n
    nl = State.Ekf.nl;
    if strcmp(Param.dataAssociation,'known')
        new = true;
        for k=1:nl
            if(State.Ekf.record(k) == Li(i))
                new = false;
                %record the position of j in record
                State.Ekf.number = k;
                correction(z(:,i));
            end
        end
        if new
            %State.Ekf.number = nl+1;
            initialize_new_landmark(z(:,i), Q);
        end
    end
    %end of known case
    if strcmp(Param.dataAssociation,'jcbb')
       
        State.Ekf.number = Li(i);
        if Li(i)==-1
            continue;
        end
        if(Li(i) ~= 0 )
            correction(z(1:2,i))
        end

        if (Li(i) == 0)
            initialize_new_landmark(z(:,i), Q);
        end
    end
    
        if strcmp(Param.dataAssociation,'nn')
       
        State.Ekf.number = Li(i);
        if Li(i)==-1
            continue;
        end
        if(Li(i) ~= 0 )
            correction(z(1:2,i))
        end

        if (Li(i) == 0)
            initialize_new_landmark(z(:,i), Q);
        end
    end

end
end
    
    

if strcmp(Param.updateMethod,'batch')
    switch lower(Param.dataAssociation)
    case 'known'
        Li = da_known(z(3,:));
        
    case 'nn'
        Li = da_nn(z(1:2,:));
    case 'jcbb'
        Li = da_jcbb(z(1:2,:), Param.R);
    otherwise
        error('unrecognized data association method: "%s"', Param.dataAssociation);
    end
    n = size(z,2);
    num = size(Li,2);

    Q = Param.R ;
    %%known case
    if strcmp(Param.dataAssociation,'known')
        ekf_number = [];
        record_z = [];
        record_new_z = [];
        
        for i=1:n
            nl = State.Ekf.nl;
            new = true;
            for k=1:nl
                if(State.Ekf.record(k) == Li(i))
                    new = false;
                    %record the position of j in record
                    ekf_number = [ekf_number, k];
                    record_z = [record_z,z(:,i)];
                end
            end
       end
    if size(find(ekf_number),2)~=0
        correction_batch(record_z,ekf_number); 
    end
    for i=1:n
        for k=1:nl
                if(State.Ekf.record(k) == Li(i))
                    new = false;
                end
        end
        if new
            initialize_new_landmark(z(:,i), Q);
        end
    end
    end
    
    %%nn case
    if strcmp(Param.dataAssociation,'nn')
        ekf_number = [];
        record_z = [];
        for i=1:n
            if Li(i) == -1
                     continue
            end
            if(Li(i) ~= 0)
                ekf_number = [ekf_number, Li(i)];
                record_z = [record_z,z(:,i)];
            end
        end

   
        if size(find(ekf_number),2)~=0
            correction_batch(record_z,ekf_number); 
        end
        for i=1:n
            if Li(i)==-1
                continue
            end
            if Li(i)==0
                initialize_new_landmark(z(:,i), Q);
            end
        end
    end

end
end



function correction_batch(z,ekf_number)
    global Param;
    global State;
    total_number = size(ekf_number,2);
    Hi = [];
    z_new_ii = [];
    Ri = zeros(2*total_number);
    Ri(1:2,1:2) = Param.R(1:2,1:2);
    if total_number > 1
        for num_R = 1:total_number-1
            Ri(2*num_R+1,2*num_R+1) = Param.R(1,1);
            Ri(2*num_R+2,2*num_R+2) = Param.R(2,2);
        end
    end
 
    for i = 1:total_number
        number_x = 2*ekf_number(i)+2;
        number_y = 2*ekf_number(i)+3;
        yita = [State.Ekf.mu(number_x) - State.Ekf.mu(1); State.Ekf.mu(number_y) - State.Ekf.mu(2)];
        q = yita'*yita;
        zt = [sqrt(q); ...
        minimizedAngle(atan2(yita(2),yita(1))- State.Ekf.mu(3))];
        F = [[eye(3);zeros(2,3)] zeros(5,2*ekf_number(i)-2) [zeros(3,2);eye(2)] zeros(5, 2*State.Ekf.nl-2*ekf_number(i))];
        H_low = 1/q*[-sqrt(q)*yita(1) -sqrt(q)*yita(2) 0 sqrt(q)*yita(1) sqrt(q)*yita(2);...
        yita(2) -yita(1) -q -yita(2) yita(1)];
        H = H_low*F;
        Hi = [Hi;H];
        z_new = [z(1,i)-zt(1);minimizedAngle(z(2,i)-zt(2))];
        z_new_ii = [z_new_ii;z_new];
    end
    
    K = State.Ekf.Sigma*Hi'*inv(Hi*State.Ekf.Sigma*Hi'+Ri);
    mu_correct = State.Ekf.mu+K*z_new_ii;
    Sigma_correct = (eye(2*State.Ekf.nl+3) - K*Hi)*State.Ekf.Sigma;
    State.Ekf.mu = mu_correct;
    State.Ekf.Sigma = Sigma_correct;
end


%%correction step for landmark have been seen
function correction(z)
    global Param;
    global State;
    number_x = 2*State.Ekf.number+2;
    number_y = 2*State.Ekf.number+3;
    yita = [State.Ekf.mu(number_x) - State.Ekf.mu(1); State.Ekf.mu(number_y) - State.Ekf.mu(2)];
    q = yita'*yita;
    zt = [sqrt(q); ...
        minimizedAngle(atan2(yita(2),yita(1))- State.Ekf.mu(3))];
    F = [[eye(3);zeros(2,3)] zeros(5,2*State.Ekf.number-2) [zeros(3,2);eye(2)] zeros(5, 2*State.Ekf.nl-2*State.Ekf.number)];
    H_low = 1/q*[-sqrt(q)*yita(1) -sqrt(q)*yita(2) 0 sqrt(q)*yita(1) sqrt(q)*yita(2);...
        yita(2) -yita(1) -q -yita(2) yita(1)];
    H = H_low*F;
    K = State.Ekf.Sigma*H'*inv(H*State.Ekf.Sigma*H'+Param.R);
    z_new = [z(1)-zt(1);minimizedAngle(z(2)-zt(2))];
    mu_correct = State.Ekf.mu+K*z_new;
    Sigma_correct = (eye(2*State.Ekf.nl+3) - K*H)*State.Ekf.Sigma;
    State.Ekf.mu = mu_correct;
    State.Ekf.Sigma = Sigma_correct;
end


