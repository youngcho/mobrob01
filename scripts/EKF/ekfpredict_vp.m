function ekfpredict_vp(u,dt)
% EKF-SLAM prediction for Victoria Park process model

global Param;
global State;

x = State.Ekf.mu(1);
y = State.Ekf.mu(2);
phi = State.Ekf.mu(3);

vc = u(1)/(1-tan(u(2))*Param.H/Param.L);
G = [1 0 dt*(-vc*sin(phi)-vc/Param.L*tan(u(2))*(Param.a*cos(phi)-Param.b*sin(phi)));...
    0 1 dt*(vc*cos(phi)+vc/Param.L*tan(u(2))*(-Param.a*sin(phi)-Param.b*cos(phi)));...
    0 0 1];
v = dt*[cos(phi)-1/Param.L*tan(u(2))*(Param.a*sin(phi)+Param.b*cos(phi)), -vc/Param.L*(1/(cos(u(2))).^2)*(Param.a*sin(phi)+Param.b*cos(phi));...
    sin(phi)+1/Param.L*tan(u(2))*(Param.a*cos(phi)+Param.b*sin(phi)), vc/Param.L*(1/(cos(u(2))).^2)*(Param.a*sin(phi)-Param.b*cos(phi));...
    1/Param.L*tan(u(2)), vc/Param.L*(1/(cos(u(2))).^2)];

%%prediction
mu = State.Ekf.mu;
mu(1:3) = State.Ekf.mu(1:3)+...
    dt*[vc*cos(phi) - vc/Param.L*tan(u(2))*(Param.a*sin(phi)+Param.b*cos(phi));...
    vc*sin(phi) + vc/Param.L*tan(u(2))*(Param.a*cos(phi)-Param.b*sin(phi));...
    vc/Param.L*tan(u(2))];
Sigma = State.Ekf.Sigma;
Sigma(1:3,1:3) = G*Sigma(1:3,1:3)*G'+v*Param.Qu*v'+Param.Qf;
Sigma(1:3,4:end) = G*Sigma(1:3,4:end);
Sigma(4:end,1:3) = Sigma(1:3,4:end)';

State.Ekf.mu = mu;
State.Ekf.Sigma = Sigma;
State.Ekf.t = State.Ekf.t+dt;




