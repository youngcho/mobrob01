function Li = da_nn(z)
% perform nearest-neighbor data association

global Param;
global State;


[num_row, num] = size(z);
Li = zeros(1,num);

if State.Ekf.nl==0
    Li = zeros(1,num);  
else

for i = 1:num
    number_x = 2+2;
    number_y = 2+3;
    yita = [State.Ekf.mu(number_x) - State.Ekf.mu(1); State.Ekf.mu(number_y) - State.Ekf.mu(2)];
    q = yita'*yita;
    F = [[eye(3);zeros(2,3)] zeros(5,2-2) [zeros(3,2);eye(2)] zeros(5, 2*State.Ekf.nl-2)];
    H_low = 1/q*[-sqrt(q)*yita(1) -sqrt(q)*yita(2) 0 sqrt(q)*yita(1) sqrt(q)*yita(2);...
    yita(2) -yita(1) -q -yita(2) yita(1)];
    H = H_low*F;
    s = H*State.Ekf.Sigma*H'+Param.R;
%     z_hat = H*State.Ekf.mu;
    z_hat = [sqrt(q); atan2(yita(2),yita(1))-State.Ekf.mu(3)];
    Distance_min = (z_hat - z(:,i))'*inv(s)*(z_hat - z(:,i));
    nearest = 1;
    
    for j = 2:State.Ekf.nl
        %j represent landmark
        number_x = 2*j+2;
        number_y = 2*j+3;
        yita = [State.Ekf.mu(number_x) - State.Ekf.mu(1); State.Ekf.mu(number_y) - State.Ekf.mu(2)];
        q = yita'*yita;
        F = [[eye(3);zeros(2,3)] zeros(5,2*j-2) [zeros(3,2);eye(2)] zeros(5, 2*State.Ekf.nl-2*j)];
        H_low = 1/q*[-sqrt(q)*yita(1) -sqrt(q)*yita(2) 0 sqrt(q)*yita(1) sqrt(q)*yita(2);...
        yita(2) -yita(1) -q -yita(2) yita(1)];
        H = H_low*F;
        s = H*State.Ekf.Sigma*H'+Param.R;
%         z_hat = H*State.Ekf.mu;
        z_hat = [sqrt(q); atan2(yita(2),yita(1))-State.Ekf.mu(3)];
        z_diff = [z_hat(1) - z(1,i); minimizedAngle(z_hat(2)-z(2,i))];
        Distance = (z_diff)'*1/s*(z_diff);
        if Distance == Distance_min
            nearest = -1;
        end
        if Distance<Distance_min
             nearest = j;
             Distance_min = Distance;
        end

    end
    %if Distance_min<=chi2inv(0.99,2)
    if Distance_min<=chi2inv(0.99,2)
        Li(i) = nearest;
    else
        li(i) = 0;
    end
end
end


    

