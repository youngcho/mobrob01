function initialize_new_landmark(z, R)

global Param;
global State;

m_new = [State.Ekf.mu(1);State.Ekf.mu(2)]+...
    z(1)*[cos(z(2)+State.Ekf.mu(3)); sin(z(2)+State.Ekf.mu(3))];

mu = [State.Ekf.mu;m_new];
G_r = [1 0 -z(1)*sin(z(2)+State.Ekf.mu(3));...
    0 1 z(1)*cos(z(2)+State.Ekf.mu(3))];
G_y = [cos(z(2)+State.Ekf.mu(3)) -z(1)*sin(z(2)+State.Ekf.mu(3));...
    sin(z(2)+State.Ekf.mu(3)) z(1)*cos(z(2)+State.Ekf.mu(3))];
SigmaX = State.Ekf.Sigma(1:3,1:3);
SigmaMX = State.Ekf.Sigma(4:end,1:3);

SigmaYnew = [SigmaX*G_r'; SigmaMX*G_r'];
SigmaMnew = G_r*SigmaX*G_r'+G_y*Param.R*G_y';
Sigma = [State.Ekf.Sigma SigmaYnew; SigmaYnew' SigmaMnew];

State.Ekf.mu = mu;
State.Ekf.Sigma = Sigma;
State.Ekf.nl = State.Ekf.nl+1;
if strcmp(Param.dataAssociation,'known')
    State.Ekf.record = [State.Ekf.record z(3)];  
end


