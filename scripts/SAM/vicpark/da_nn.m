function [Li, mu, Sigma, margTime] = da_nn(mu, marg, Hxl, Q, z, thresh, t)
% perform nearest-neighbor data association

%PRINCE - Jx and Jz need to be the funciton handles here. Not the evaluted
%Jacobians

global Param;
global State;

import gtsam.*



margTime = 0;

if(State.Ekf.nL == 0)
    Li = 1:size(z,2);
    Sigma = [];
    return;
end

%Robot State
robInd = 1:3;
robState = mu(robInd);

Li = zeros(size(z,2), 1);
%Store all computed mahalanobis distances
mahaDist = zeros(size(z,2), State.Ekf.nL);





%Iterate over all measurements
for i = 1:size(z,2)
    zi = z(:,i);
    
    %Iterate over all known landmarks
    for j = 1:State.Ekf.nL
        Lj = symbol('L', j);
        xt = symbol('x', t);
        
        try
%             tic;
            startTime = toc;
            Sigma = [marg.at(xt,xt), marg.at(xt,Lj);
                     marg.at(Lj, xt), marg.at(Lj,Lj)];
            margTime = margTime + toc - startTime;
        catch
            keyboard;
        end
                 
        %Get j'th landmark
        lmInd = 3 + 2*j - 1;        %Index of X component
        lmInd = lmInd:lmInd + 1;    %Index of X and Y components
        lmState = mu(lmInd);
        
        %Get Jacobian of this measurement
        dx = lmState(1) - robState(1);
        dy = lmState(2) - robState(2);
        Hj = Hxl(dx, dy);
        
        %Estimate measurement based on this landmark
        zHat = observation_vp_nn(robState, lmState);
        
        %Innovation and Covariance
        inno = [zi(1) - zHat(1);
                minimizedAngle(zi(2) - zHat(2))];
        S = Hj * Sigma * Hj' + Q;
        
        %Mahalanobis Distance
        mahaDist(i, j) = (inno' / S) * inno;
    end       
end

    costMat = mahaDist;
	m_idx = 1 : size(z,2); % index of measurement
	while 1
		done = true;
		Li_tmp = [];
		[assign, cost] = munkres(costMat);
		% get assignment
		for i = 1 : size(assign,1)
			idx = find(assign(i,:));

			% if not assign, probably a new landmark
			if isempty(idx)
				% spurious measurement
				Li_tmp = [Li_tmp, State.Ekf.nL+1];
				continue;
			end

			% check ambiguilty
            try
			cost = costMat(i, :);
            catch 
                keyboard;
            end
			if nnz(abs(cost - costMat(i,idx)) < 0.5) >= 2
				costMat(i,:) = []; % discard this measurement
				Li(m_idx(i)) = -1; % discarded measurement marked as -1
				m_idx(i) = [];
				done = false;
				break;
			end

			% check compartibility
			if costMat(i,idx) < thresh
				Li_tmp = [Li_tmp, idx];
			else
				% new landmark
				Li_tmp = [Li_tmp, State.Ekf.nL+1];
			end
		end		

		if done 
			break;
		end

	end

	% copy Li_tmp into Li
	j = 1;
	num_mapped = State.Ekf.nL;
	for i = 1 : length(Li)
		if Li(i) == -1
			continue;
        else
            try
			if Li_tmp(j) > State.Ekf.nL
				Li(i) = num_mapped + 1;
				num_mapped = num_mapped + 1;
			else
				Li(i) = Li_tmp(j);
			end
			j = j + 1;
            catch 
                keyboard
            end
		end
	end
% %Do some association with the mahaDists compute
% keyIndeces = munkres(mahaDist);
% 
% 
% %Make new landmarks if selected MahaDist == threshold
% sigCounter = 1;
% for i = 1:size(z,2)
% 
%    col = find(keyIndeces(i,:));
%    maha = mahaDist(i, col);
%     
%    % If the best landmark for this measuremnet is larger than thresh,
%    % make a new landmark
%    if(maha >= thresh)
%        s(i) = State.Ekf.nL + sigCounter;
%        sigCounter = sigCounter + 1;
%    else
%        try
%         s(i) = col;
%        catch
%         keyboard
%        end
%    end
% end


