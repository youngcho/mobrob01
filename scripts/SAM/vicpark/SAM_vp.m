function SAM_vp(nSteps,pauseLen)

import gtsam.*

% create an empty factor graph
graph = NonlinearFactorGraph;
graphNew = NonlinearFactorGraph;

%Initial guess for each pose and landmark
initial = Values;
initialNew = Values;

%iSAM solver
isam = ISAM2;

global Param;
global State;
global Data;

State.Ekf.nL = 0;

if ~exist('nSteps','var') || isempty(nSteps)
    nSteps = 600;
end

if ~exist('pauseLen','var')
    pauseLen = 0; % seconds
end

Data = load_vp_si();

%===================================================
% Initalize Params
%===================================================
% vehicle geometry
Param.a = 3.78; % [m]
Param.b = 0.50; % [m]
Param.L = 2.83; % [m]
Param.H = 0.76; % [m]

% 2x2 process noise on control input
sigma.vc = 0.02; % [m/s]
sigma.alpha = 2*pi/180; % [rad]
Param.Qu = diag([sigma.vc, sigma.alpha].^2);

% 3x3 process noise on model error
sigma.x = 0.1; % [m]
sigma.y = 0.1; % [m]
sigma.phi = 0.5*pi/180; % [rad]
Param.Qf = diag([sigma.x, sigma.y, sigma.phi].^2);

% 2x2 observation noise
sigma.r = 0.05; % [m]
sigma.beta = 1*pi/180; % [rad]
Param.R = diag([sigma.r, sigma.beta].^2);
%===================================================
% Useful Function Handles
%===================================================
%Motion model WRT input                               
Fu = @(dt, x, u, a, b, L) ...
 dt * [ (cos(x(3)) - 1/L * tan(u(2)))*(a * sin(x(3)) + b * cos(x(3))),  -u(1)*(a*sin(x(3)) + b*cos(x(3)))/L/cos(u(2))^2;
       (sin(x(3)) + 1/L * tan(u(2)))*(a * cos(x(3)) - b * sin(x(3))),  u(1)*(a*cos(x(3)) - b*sin(x(3)))/L/cos(u(2))^2;
        1/L * tan(u(2)),    u(1)/L / cos(u(2))^2];
    
% Measurement Model linearized WRT state
Hx = @(dx, dy) [-dx /sqrt(dx^2+dy^2),    -dy/sqrt(dx^2+dy^2),  0;
                dy /(dx^2+dy^2),         -dx/(dx^2+dy^2),     -1];
            
% Measurement Model linearized WRT landmark
Hl = @(dx, dy) [dx /sqrt(dx^2+dy^2),    dy/sqrt(dx^2+dy^2);
                -dy /(dx^2+dy^2),         dx/(dx^2+dy^2)];
            
% Measurement Model linearized WRT state AND landmark
Hxl = @(dx, dy) [Hx(dx, dy), Hl(dx, dy)];

%Measurement Noise
Q = Param.R;

%===================================================
%% Initialize State
%===================================================
State.Ekf.mu = [Data.Gps.x(2), Data.Gps.y(2), 36*pi/180]';
xNew = State.Ekf.mu;
State.Ekf.Sigma = zeros(3);


global AAr;
AAr = [0:360]*pi/360;

ci = 1; % control index
kStart = 1;

% Add a Gaussian prior on pose x_1
priorMean = Pose2(State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.mu(3));
priorNoise = noiseModel.Diagonal.Sigmas([0; 0; 0]);
graph.add(PriorFactorPose2(symbol('x', ci), priorMean, priorNoise)); % add directly to graph
graphNew.add(PriorFactorPose2(symbol('x', ci), priorMean, priorNoise)); % add directly to graph

%Add Initial pose (prior pose) to initial guess
initial.insert(symbol('x', ci), priorMean);
initialNew.insert(symbol('x', ci), priorMean);

%Measurement Noise (Bearing then Range)
measNoiseVec = [sigma.beta; sigma.r];
measNoise = noiseModel.Diagonal.Sigmas(measNoiseVec);

%Data association Maha Dist Threshold
nnThresh = chi2inv(0.99,2);

%Initiliaze mu and marginals
mu = State.Ekf.mu;
marg = [];

%Number of iterations (k) before we start using iSAM
batchNum = 1;
% batchNum = min(nSteps, length(Data.Laser.time));

%Computation Time
compTime = zeros(3, nSteps);
compTimek = 0;

%Counter to toggle when to re-run a batch update
iCount = 1;
iCountMax = 0;

%Video Stuff
makeVideo = 0;
if ~exist('makeVideo','var')
    makeVideo = 0; 
end

if makeVideo
    try
        votype = 'avifile';
        vo = avifile('video.avi', 'fps', min(5, 1/pauseLen));
    catch
        votype = 'VideoWriter';
        vo = VideoWriter('video', 'MPEG-4');
        set(vo, 'FrameRate', min(5, 1/pauseLen));
        open(vo);

        vo2 = VideoWriter('video2', 'MPEG-4');
        set(vo2, 'FrameRate', min(5, 1/pauseLen));
        open(vo2);
    end
end


%===================================================
% Main Loop
%===================================================
figure(1); clf;
axis equal;
cnt =  1;

LmList = [];
LmCnt = [];

t = min(Data.Laser.time(1), Data.Control.time(1));
for k=kStart:min(nSteps, length(Data.Laser.time))
    %% Prediction (odometry)
    compTimek = 0;
    while (Data.Control.time(ci) < Data.Laser.time(k))
       % control available
       dt = Data.Control.time(ci) - t;
       t = Data.Control.time(ci);
       
       vc = Data.Control.ve(ci) / (1 - tan(Data.Control.alpha(ci)) * ...
            Param.H / Param.L);
       u = [vc, Data.Control.alpha(ci)]';
       
       %Get odoemtry Noise
       Fu_t = Fu(dt, State.Ekf.mu(1:3), u, Param.a, Param.b, Param.L);
       odoNoise = Fu_t * Param.Qu * Fu_t' + Param.Qf;
       odoNoiseMatrix = noiseModel.Gaussian.Covariance(odoNoise);

       xNew = predict_vp_SAM(State.Ekf.mu, u, dt);
       dS = xNew - State.Ekf.mu(1:3);
       
       dR = norm(dS(1:2));
       dTh = minimizedAngle(xNew(3) - State.Ekf.mu(3));
       
       %Add odometry factor to graph 
       alph = atan2(dS(2), dS(1));
       phi = alph - State.Ekf.mu(3);
       odometry =  Pose2(dR * cos(phi), dR * sin(phi), dTh);
       graph.add(BetweenFactorPose2(symbol('x',ci), symbol('x', ci+1), ...
                                    odometry, odoNoiseMatrix));
    graphNew.add(BetweenFactorPose2(symbol('x',ci), symbol('x', ci+1), ...
                                    odometry, odoNoiseMatrix));

       %Add predicted state to initial guess
       predNextPose = Pose2(xNew(1), xNew(2), xNew(3));
       initial.insert(symbol('x', ci+1), predNextPose);
       initialNew.insert(symbol('x', ci+1), predNextPose);

       %Increment Iterator. UPdate state
       ci = ci+1;
       State.Ekf.mu(1:3) = xNew;

    end

    tic;
    if (ci >= 1 && (k <= batchNum || ~mod(iCount,iCountMax)))
       batchOptimizer = LevenbergMarquardtOptimizer(graph, initial);
       result = batchOptimizer.optimize();
%        initial = result;
        
%        initialNew = result;
%        graphNew = graph;
%         isam = ISAM2;
    else
       isam.update(graphNew, initialNew);
       result = isam.calculateBestEstimate();
       
%        initial = result;
       
       initialNew = Values;
       graphNew = NonlinearFactorGraph;
    end
    compTime(1,k) = toc;
    compTimek = compTimek + toc;
    
    
    % observation available
    dt = Data.Laser.time(k) - t;
    t = Data.Laser.time(k);
    z = detectTreesI16(Data.Laser.ranges(k,:));

    
    %% NN and Update
    [mu, marg] = ...
        prepareNearestNeighbor(ci, 1:State.Ekf.nL, graph, initial);
%     [mu, marg] = ...
%        prepareNearestNeighbor(ci, 1:State.Ekf.nL, graph, result);

    tic;
    [Li, mu, Sigma, margTime] = da_nn(mu, marg, Hxl, Q, z, nnThresh, ci);
    daNnTime = toc;
    compTime(3,k) = daNnTime - margTime;
    compTimek = compTimek + toc;
    
    
    for i = 1:size(z,2)
        if Li(i) < 0
            continue;
        end
        lmIsNew = Li(i) > State.Ekf.nL;
        if(lmIsNew)
            %Compute 
            lm = z(1,i) * [cos(z(2,i) + xNew(3) - pi/2);
                           sin(z(2,i) + xNew(3) - pi/2)];

            lm = lm + [xNew(1);  
                       xNew(2)];
            
            %Add initial guess for landmark factor
            Li(i) = State.Ekf.nL + 1;
            initial.insert(symbol('L',Li(i)),Point2(lm(1),lm(2)));
            initialNew.insert(symbol('L',Li(i)),Point2(lm(1),lm(2)));

            %Update Global variables to maintain invariance
            State.Ekf.nL = State.Ekf.nL + 1;
        end
        
        %Add measurement to graph
        poseID = symbol('x', ci);
        lmID = symbol('L', Li(i));
        angle = minimizedAngle(z(2,i) - pi/2);
        range = z(1,i);
        
        %Add landmark factor
        graph.add(BearingRangeFactor2D(poseID, lmID, ...
                                       Rot2(angle), range, measNoise));

        graphNew.add(BearingRangeFactor2D(poseID, lmID, ...
                                       Rot2(angle), range, measNoise));
    end
    
    
    %% Calculate a Soltuion from optimizer
    
    tic;
    if(k <= batchNum || ~mod(iCount,iCountMax) )
        batchOptimizer = LevenbergMarquardtOptimizer(graph, initial);
        result = batchOptimizer.optimize();

        iCount = 1;
    else
        isam.update(graphNew, initialNew);
        result = isam.calculateBestEstimate();
        
        initialNew = Values;
        graphNew = NonlinearFactorGraph;
        
        iCount = iCount + 1;
    end
    initial = result;
    State.Ekf.mu(1:3) = [result.at(symbol('x',ci)).x; result.at(symbol('x',ci)).y; result.at(symbol('x',ci)).theta];
    compTime(2,k) = toc;
    compTimek = compTimek + toc;
%     compTime(k) = compTimek;

    mar = Marginals(graph, result);
    
    %% Graph things
    figure(1);
    XYT = utilities.extractPose2(result);
    doGraphics(z, XYT);
    plotcov2d( XYT(end,1), XYT(end,2), mar.marginalCovariance(symbol('x', ci)), 'blue', 0, 'blue', 0, 3);
    
    %Plot Estimated Trajectory (and landmark positions)
    XYT = utilities.extractPose2(result);
    plot(XYT(:,1),XYT(:,2),'k-');

    for i = 1 : State.Ekf.nL
        plotcov2d( result.at(symbol('L',i)).x, result.at(symbol('L',i)).y, mar.marginalCovariance(symbol('L', i)), 'red', 0, 'red', 0, 3);
    end
    hold off;

    for i = 1 : length(Data.Gps.time)
      if Data.Gps.time(i) > t
        break;
      end
    end

    if i >= length(Data.Gps.time)
      warning('Gps used up\n');
    end
    
    hold on;
    gt_traj = [Data.Gps.x(1:max(1,i-1))'; Data.Gps.y(1:max(1,i-1))'];
    scatter(gt_traj(1,:), gt_traj(2,:), 'g^');
    hold off;
    drawnow;
    
    figure(2);
    clf;
    hold on;
    area([compTime(1, 1:k); compTime(2, 1:k); compTime(3, 1:k)]', ...
         'FaceColor', 'flat', 'linewidth', 1.2);
    grid;
    hold off;
    xlabel('Iteration Number', 'Fontsize', 17)
    ylabel('Computation Time [s]', 'Fontsize', 17)
    leg = legend('Optimizer on Odometry', 'Optimizer on Measurement', ...
          'Nearest Neighbor', 'Location', 'NorthWest');
    leg.FontSize = 17;
    drawnow;


    %Record frame for video
    if makeVideo
        
        figure(1);
        
        F = getframe(gcf);
        
        figure(2);
        
        F2 = getframe(gcf);
        switch votype
          case 'avifile'
            vo = addframe(vo, F);
          case 'VideoWriter'
            writeVideo(vo, F);
            writeVideo(vo2, F2);
          otherwise
            error('unrecognized votype');
        end
    end
end

if makeVideo
    fprintf('Writing video...');
    switch votype
      case 'avifile'
        vo = close(vo);
      case 'VideoWriter'
        close(vo);
        close(vo2);
      otherwise
        error('unrecognized votype');
    end
    fprintf('done\n');
end

%% Functions
%==========================================================================
function doGraphics(z,XYT)
% Put whatever graphics you want here for visualization
%
% WARNING: this slows down your process time, so use sparingly when trying
% to crunch the whole data set!

global Param;
global State;

% plot the robot and 3-sigma covariance ellipsoid
plotbot(XYT(end,1), XYT(end,2), XYT(end,3), 'black', 1, 'blue', 1);
hold on;

% restrict view to a bounding box around the current pose
BB=30;
axis([[-BB,BB]+XYT(end,1), [-BB,BB]+XYT(end,2)]);
% axis auto


% project raw sensor detections in global frame using estimate pose
xr = XYT(end,1);
yr = XYT(end,2);
tr = XYT(end,3);
for k=1:size(z,2)
    r = z(1,k);
    b = z(2,k);
    xl = xr + r*cos(b+tr-pi/2);
    yl = yr + r*sin(b+tr-pi/2);
    plot([xr; xl], [yr; yl],'r',xl,yl,'r*');
end


% hold off;

