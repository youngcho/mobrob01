function [muAug, jointMargs] = prepareNearestNeighbor(t, sL, graph, result)
%PREPARENEARESTNEIGHBOR Summary of this function goes here
%   Detailed explanation goes here

    import gtsam.*

    %Initialize
    lmIndex = zeros(1, length(sL));
    muAug = zeros(3 + 2 * length(sL), 1);

    %Make Pose and Landmark keys and keyvect
    poseIndex = symbol('x', t);
    
    robLandKeyVec = utilities.createKeyVector('x', t);

    for i = 1:length(lmIndex)
       lmIndex(i) =  symbol('L', sL(i));
       robLandKeyVec.push_back(symbol('L', sL(i)));
    end
    
    %% Construct Augmented state
    muAug(1:3) = [result.at(poseIndex).x;
                  result.at(poseIndex).y;
                  result.at(poseIndex).theta];
              
    for i = 1:length(lmIndex)
       dex = 3 + 2 * i - 1 ;
       dex = dex:dex+1;
       
       muAug(dex) = [result.at(symbol('L', sL(i))).x;
                     result.at(symbol('L', sL(i))).y;];
    end
    
    %% Construct Augmented Covariance
%     printKeyVector(robLandKeyVec)
    mar = Marginals(graph, result);
    jointMargs = mar.jointMarginalCovariance(robLandKeyVec);
    

