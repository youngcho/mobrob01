function [s, mu, Sigma] = da_nn(mu, marg, Hxl, Jx, Jz, Q, z, thresh, t)
% perform nearest-neighbor data association

%PRINCE - Jx and Jz need to be the funciton handles here. Not the evaluted
%Jacobians

global Param;
global State;

import gtsam.*



if(State.Ekf.nL == 0)
    s = z(3,:);
    Sigma = [];
    mu = [];
    return;
end

%Robot State
robInd = 1:3;
robState = mu(robInd);

s = zeros(size(z,2), 1);
%Store all computed mahalanobis distances
mahaDist = thresh * ones(size(z,2), State.Ekf.nL + 1);
    



%Iterate over all measurements
for i = 1:size(z,2)
    zi = z(:,i);
    
    %Add id to truth set
    lmIsNew = ~sum(zi(3) == State.Ekf.iM);
    if(lmIsNew)
        State.Ekf.iM = [State.Ekf.iM, zi(3)]; 
    end
  
    %Iterate over all known landmarks
    for j = 1:State.Ekf.nL
        Lj = symbol('L', State.Ekf.sL(j));
        xt = symbol('x', t);
        
        try
            Sigma = [marg.at(xt,xt), marg.at(xt,Lj);
                     marg.at(Lj, xt), marg.at(Lj,Lj)];
        catch
            keyboard;
        end
                 
        
        
        
        
        
        
        %Get j'th landmark
        lmInd = 3 + 2*j - 1;        %Index of X component
        lmInd = lmInd:lmInd + 1;    %Index of X and Y components
        lmState = mu(lmInd);
        
        %Augmented Linearized Measurement Model Matrix
%         Fz = [eye(3), zeros(3, 2*State.Ekf.nL);
%               zeros(2, 3), zeros(2, 2*State.Ekf.nL)];
%         Fz(4:5, lmInd) = eye(2);
        
        %Get Jacobian of this measurement
        dx = lmState(1) - robState(1);
        dy = lmState(2) - robState(2);
        Hj = Hxl(dx, dy);
%         bigHj = Hj * Fz;
        
        
        
        %Estimate measurement based on this landmark
        zHat = observation_sim_nn(robState, lmState);
        
        %Innovation and Covariance
        inno = zi(1:2) - zHat;
        S = Hj * Sigma * Hj' + Q;
        
        %Mahalanobis Distance
        mahaDist(i, j) = (inno' / S) * inno;
    end       
end

%Do some association with the mahaDists compute
keyIndeces = munkres(mahaDist);
% mahaDistNew = mahaDist(keyIndeces);
%Make new landmarks if selected MahaDist == threshold
for i = 1:size(z,2)
%    maha = mahaDistNew(i);
   col = find(keyIndeces(i,:));
   maha = mahaDist(i, col);
    
   % If the best landmark for this measuremnet is larger than thresh,
   % make a new landmark
   if(maha >= thresh)
       zi = z(:,i);
       s(i) = zi(3);
   else
       try
        s(i) = State.Ekf.sL(col);
       catch
           keyboard
       end
   end
   
   
    
end


