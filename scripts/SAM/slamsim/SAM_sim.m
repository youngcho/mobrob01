function varargout = SAM_sim()

% import gtsam library
import gtsam.*

% create an empty factor graph and initial node guesses
graph = NonlinearFactorGraph;
initial = Values;
graphNew = NonlinearFactorGraph;
initialNew = Values;

isam = ISAM2;


global Param;
global Data;
global State;

State.Ekf.iL    = [];         % nL cell array containing indices of landmark i
State.Ekf.sL    = [];         % nL vector containing signatures of landmarks
State.Ekf.nL    = 0;          % scalar number of landmarks
State.Ekf.iM    = [];

% Parameters for how many steps of simulations to take
stepsOrData = 200;

if ~exist('pauseLen','var')
    pauseLen = 0.0; % seconds
end

% Initalize Params
%===================================================
Param.choice = 'sim';

Param.initialStateMean = [180 50 0]';

% max number of landmark observations per timestep
Param.maxObs = 2;

% number of landmarks per sideline of field (minimum is 3)
Param.nLandmarksPerSide = 4;

% Motion noise (in odometry space, see p.134 in book).
Param.alphas = [0.05 0.001 0.05 0.01].^2; % std of noise proportional to alphas

% Standard deviation of Gaussian sensor noise (independent of distance)
Param.beta = [10, deg2rad(10)]; % [cm, rad]
Param.R = diag(Param.beta.^2);

% Step size between filter updates, can be less than 1.
Param.deltaT=0.1; % [s]

if isscalar(stepsOrData)
    % Generate a data set of motion and sensor info consistent with
    % noise models.
    numSteps = stepsOrData;
    Data = generateScript(Param.initialStateMean, numSteps, Param.maxObs, Param.alphas, Param.beta, Param.deltaT);
else
    % use a user supplied data set from a previous run
    Data = stepsOrData;
    numSteps = size(Data, 1);
    global FIELDINFO;
    FIELDINFO = getfieldinfo;
end

%===================================================

%% Initialize State
%===================================================
State.Ekf.mu = Param.initialStateMean;
State.Ekf.Sigma = zeros(3);

% Add a Gaussian prior on pose x_1
priorMean = Pose2(State.Ekf.mu(1), State.Ekf.mu(2), State.Ekf.mu(3));
priorNoise = noiseModel.Diagonal.Sigmas([0.0; 0.0; 0.0]);
graph.add(PriorFactorPose2(symbol('x', 1), priorMean, priorNoise)); % add directly to graph
graphNew.add(PriorFactorPose2(symbol('x', 1), priorMean, priorNoise)); % add directly to graph

%Add Initial pose (prior pose) to initial guess
initial.insert(symbol('x', 1), priorMean);
initialNew.insert(symbol('x', 1), priorMean);

%Measurement Noise (Bearing then Range)
measNoiseVec = [Param.beta(2);Param.beta(1)];    
measNoise = noiseModel.Diagonal.Sigmas(measNoiseVec);

%Add landmarks to graph
% for i=1:8
% %   initial.insert(symbol('L',j),Point2(sigmaInitial*randn,sigmaInitial*randn));
%     initial.insert(symbol('L',i),Point2(0,0));
% end

augMu = [];
jointMarg = [];

nnThresh = chi2inv(.99, 2);


%===================================================

%% Useful Function Handles
%===================================================
% % Motion Model linearized WRT state
% Gx = @(x, u) [1,    0,  -u(2)* sin(u(1) + x(3));
%               0,    1,   u(2)* cos(u(1) + x(3));
%               0,    0,           1              ];
%           
% % Motion Model linearized WRT input
% Gu = @(x, u) [-u(2)* sin(u(1) + x(3)),  cos(u(1) + x(3)),   0;
%               u(2)* cos(u(1) + x(3)),   sin(u(1) + x(3)),   0;
%                         1,                      0,          1];
         
% Measurement Model linearized WRT state
Hx = @(dx, dy) [-dx /sqrt(dx^2+dy^2),    -dy/sqrt(dx^2+dy^2),  0;
                dy /(dx^2+dy^2),         -dx/(dx^2+dy^2),     -1];
            
% Measurement Model linearized WRT landmark
Hl = @(dx, dy) [dx /sqrt(dx^2+dy^2),    dy/sqrt(dx^2+dy^2);
                -dy /(dx^2+dy^2),         dx/(dx^2+dy^2)];
            
% Measurement Model linearized WRT state AND landmark
Hxl = @(dx, dy) [Hx(dx, dy), Hl(dx, dy)];

%Landmark Init WRT state
Jx = @(x, z) [1,    0,      -z(1) * sin(x(3) + z(2));
              0,    1,       z(1) * cos(x(3) + z(2))]; 
          
%Landmark Init WRT measurement
Jz = @(x, z) [cos(x(3) + z(2)),    -z(1) * sin(x(3) + z(2));
              sin(x(3) + z(2)),     z(1) * cos(x(3) + z(2))]; 
% Motion Model Noise [a = alphas, u = RTR commands]
M = @(a, u) [a(1) * u(1)^2 + a(2) * u(2)^2,     0,      0;
             0,     a(3) * u(2)^2 + a(4)*(u(1)^2 + u(3)^2),     0;
             0,     0,  a(1)*u(3)^2 + a(2)*u(2)^2];
         
% Motion Noise Mapping (from RTR to dX, dY, dTh)         
V = @(th, u) [-u(2)* sin(th + u(1)) ,     cos(th + u(1)),    0;
               u(2)* cos(th + u(1)) ,     sin(th + u(1)),    0;
                                   1,                   0,    1];
                               
                               
Q = Param.R;
               

%% Run Main Loop
k = 1;
Kmin = numSteps;
p = 1;
pMax = 2;
for t = 1:numSteps
    plotsim(t);

    %=================================================
    % data available to your filter at this time step
    %=================================================
    u = getControl(t);
    z = getObservations(t);


    %=================================================
    %TODO: update your factor graph
    %=================================================
    
    %Create  a GTSAM matrix of our odometry noise
    Vt = V(State.Ekf.mu(3), u);
    Mt = M(Param.alphas, u);  %Get odometry noise Matrix
    odoNoise = Vt * Mt * Vt';
    odoNoiseMatrix = noiseModel.Gaussian.Covariance(odoNoise);
    
    %Get Change in x, y, and heading from prediciton function
    xNew = prediction(State.Ekf.mu(1:3), u);
    deltaState = xNew - State.Ekf.mu(1:3);
    
    dR = norm(deltaState(1:2));
    dTh = minimizedAngle(xNew(3) - State.Ekf.mu(3));    
 
    %Add odometry factor to graph 
    dS = deltaState;
    alph = atan2(dS(2), dS(1));
    phi = alph - State.Ekf.mu(3);
    odometry =  Pose2(dR * cos(phi), dR * sin(phi), dTh);
    graph.add(BetweenFactorPose2(symbol('x',t), symbol('x', t+1), ...
                                 odometry, odoNoiseMatrix));
    graphNew.add(BetweenFactorPose2(symbol('x',t), symbol('x', t+1), ...
                                 odometry, odoNoiseMatrix));
                             
    %Add predicted state to initial guess
    predNextPose = Pose2(xNew(1), xNew(2), xNew(3));
    initial.insert(symbol('x', t+1), predNextPose);
    initialNew.insert(symbol('x', t+1), predNextPose);
    
    %Increment State
    State.Ekf.mu  = xNew;
    
    %Do Data association
    [Li mu, Sigma] = da_nn(augMu, jointMarg, Hxl, Jx, Jz, Q, z, nnThresh, t);
    
    for i = 1:size(z,2)
        lmIsNew = ~sum(Li(i) == State.Ekf.sL);
        if(lmIsNew)
            %Compute 
            lm = z(1,i) * [cos(z(2,i) + xNew(3));
                           sin(z(2,i) + xNew(3))];

            lm = lm + [xNew(1);  
                       xNew(2)];
                   
            %Add initial guess for landmark factor
            initial.insert(symbol('L',Li(i)),Point2(lm(1),lm(2)));
            initialNew.insert(symbol('L',Li(i)),Point2(lm(1),lm(2)));

            %Update Global variables to maintain invariance
            State.Ekf.nL = State.Ekf.nL + 1;
            State.Ekf.iL = [State.Ekf.iL, State.Ekf.nL];
            State.Ekf.sL = [State.Ekf.sL, Li(i)];
        end
        
        %Add measurement to graph
        poseID = symbol('x', t+1);
        lmID = symbol('L', Li(i));
        angle = z(2,i);
        range = z(1,i);
        
        %Add landmark factor
        graph.add(BearingRangeFactor2D(poseID, lmID, ...
                                       Rot2(angle), range, measNoise));
        graphNew.add(BearingRangeFactor2D(poseID, lmID, ...
                                       Rot2(angle), range, measNoise));
    end
        
    %=================================================
    %TODO: plot and evaluate filter results here
    %=================================================
%     plot2DTrajectory(initial, 'r-o'); 

    %% Run ISAM
%     if batchInitialization % Do a full optimize for first minK ranges
%       batchOptimizer = LevenbergMarquardtOptimizer(graph, initial);
%       initial = batchOptimizer.optimize();
%       batchInitialization = false; % only once
%     end  

    if k < Kmin || ~(mod(t, pMax))
        batchOptimizer = LevenbergMarquardtOptimizer(graph, initial);
        result = batchOptimizer.optimize();
%         lastPose = result.at(symbol('x', t+1));
%         State.Ekf.mu = [lastPose.x, lastPose.y, lastPose.theta]';

        [augMu, jointMarg] = ...
            prepareNearestNeighbor(t, State.Ekf.sL, graph, result);
        
        initialNew = result;
        graphNew = graph;
        isam = ISAM2;
        k = k + 1;
        p = 1;
    else
        try    
            isam.update(graphNew, initialNew);
        catch
            hello = 'what'
        end
%         isam.update(graphNew, initialNew);

        result = isam.calculateEstimate();
        
        
        
        lastPose = result.at(symbol('x', t+1));
        State.Ekf.mu = [lastPose.x, lastPose.y, lastPose.theta]'; 
        graphNew = NonlinearFactorGraph;
        initialNew = Values;
        p = p + 1;
    end
    
%     %% Run Optimizer
%     batchOptimizer = LevenbergMarquardtOptimizer(graph, initial);
%     result = batchOptimizer.optimize();
%     marginals = Marginals(graph, result);

    %Plot Odometry
    % XYT = utilities.extractPose2(initial);
    % plot(XYT(:,1),XYT(:,2),'y-');

    %Plot Estimated Trajectory (and landmark positions)
    XYT = utilities.extractPose2(result);
    plot(XYT(:,1),XYT(:,2),'k-');
    XY = utilities.extractPoint2(result);
    plot(XY(:,1),XY(:,2),'k*');
    
    drawnow;
    if pauseLen > 0
        pause(pauseLen);
    end
end

% graph.print(sprintf('\nFactor graph:\n'));

%Use GTSAM optimizer to solve graph
% batchOptimizer = LevenbergMarquardtOptimizer(graph, initial);
% result = batchOptimizer.optimize();
% 
% %Plot Odometry
% XYT = utilities.extractPose2(initial);
% plot(XYT(:,1),XYT(:,2),'y-');
% 
% %Plot Estimated Trajectory (and landmark positions)
% XYT = utilities.extractPose2(result);
% plot(XYT(:,1),XYT(:,2),'k-');
% XY = utilities.extractPoint2(result);
% plot(XY(:,1),XY(:,2),'k*');
% 
if nargout >= 1
    varargout{1} = Data;
end

%==========================================================================
function u = getControl(t)
global Data;
% noisefree control command
u = Data.noisefreeControl(:,t);  % 3x1 [drot1; dtrans; drot2]


%==========================================================================
function z = getObservations(t)
global Data;
% noisy observations
z = Data.realObservation(:,:,t); % 3xn [range; bearing; landmark id]
ii = find(~isnan(z(1,:)));
z = z(:, ii);

%==========================================================================
function plotsim(t)
global Data;

%--------------------------------------------------------------
% Graphics
%--------------------------------------------------------------

NOISEFREE_PATH_COL = 'green';
ACTUAL_PATH_COL = 'blue';

NOISEFREE_BEARING_COLOR = 'cyan';
OBSERVED_BEARING_COLOR = 'red';

GLOBAL_FIGURE = 1;

%=================================================
% data *not* available to your filter, i.e., known
% only by the simulator, useful for making error plots
%=================================================
% actual position (i.e., ground truth)
x = Data.Sim.realRobot(1,t);
y = Data.Sim.realRobot(2,t);
theta = Data.Sim.realRobot(3,t);

% real observation
observation = Data.realObservation(:,:,t);

% noisefree observation
noisefreeObservation = Data.Sim.noisefreeObservation(:,:,t);

%=================================================
% graphics
%=================================================
figure(GLOBAL_FIGURE); clf; hold on; plotfield(observation(3,:));

% draw actual path (i.e., ground truth)
plot(Data.Sim.realRobot(1,1:t), Data.Sim.realRobot(2,1:t), 'Color', ACTUAL_PATH_COL);
plotrobot( x, y, theta, 'black', 1, ACTUAL_PATH_COL);

% draw noise free motion command path
plot(Data.Sim.noisefreeRobot(1,1:t), Data.Sim.noisefreeRobot(2,1:t), 'Color', NOISEFREE_PATH_COL);
plot(Data.Sim.noisefreeRobot(1,t), Data.Sim.noisefreeRobot(2,t), '*', 'Color', NOISEFREE_PATH_COL);

for k=1:size(observation,2)
    rng = Data.Sim.noisefreeObservation(1,k,t);
    ang = Data.Sim.noisefreeObservation(2,k,t);
    noisy_rng = observation(1,k);
    noisy_ang = observation(2,k);

    % indicate observed range and angle relative to actual position
    plot([x x+cos(theta+noisy_ang)*noisy_rng], [y y+sin(theta+noisy_ang)*noisy_rng], 'Color', OBSERVED_BEARING_COLOR);

    % indicate ideal noise-free range and angle relative to actual position
    plot([x x+cos(theta+ang)*rng], [y y+sin(theta+ang)*rng], 'Color', NOISEFREE_BEARING_COLOR);
end

% robLandKeyVec = utilities.createKeyVector('x', 5);
% robLandKeyVec.push_back(symbol('L',3))
% robLandKeyVec.push_back(symbol('L',4))
% printKeyVector(robLandKeyVec)
% moreMarginals = marginals.jointMarginalInformation(robLandKeyVec)
% L3 = symbol('L',3)
% L4 = symbol('L',4)
% x5 = symbol('x',5)
% moreMarginals.at(x5,L4)
% moreMarginals.at(L4,x5)