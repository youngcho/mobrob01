%-------------------------------------------------------
% returns observation of the specified landmark given the current state
%-------------------------------------------------------
function obs = observation_sim_nn( state, lm)

% Compute expected observation.
dx = lm(1) - state(1);
dy = lm(2) - state(2);
dist = sqrt( dx^2 + dy^2);

obs = [	dist; ...
        minimizedAngle(atan2(dy, dx) - state(3))];
